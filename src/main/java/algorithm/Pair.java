package algorithm;

public class Pair {

	private final Person young;
	private final Person old;
	private final long ageDifference;

	private Pair(Person person1, Person person2) {
		if (person1.isYounguerThan(person2)) {
			this.young = person1;
			this.old = person2;
		} else {
			this.young = person2;
			this.old = person1;
		}

		this.ageDifference = person1.ageDifferenceWith(person2);
	}

	public static Pair create(Person person1, Person person2) {
		return new Pair(person1, person2);
	}

	public Person getYoung() {
		return this.young;
	}

	public Person getOld() {
		return this.old;
	}

	public long getAgeDifference() {
		return this.ageDifference;
	}

}
