package algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Finder {
	private final List<Person> people;

	public Finder(List<Person> people) {
		this.people = people;
	}

	public Optional<Pair> find(MatchCriteria matchCriteria) {
		List<Pair> pairs = this.getPairs();
		Optional<Pair> bestMatch = this.getBestMatch(pairs, matchCriteria);
		return bestMatch;
	}

	private List<Pair> getPairs() {
		List<Pair> pairs = new ArrayList<Pair>();

		this.people.stream()
				.forEach(
						currentPerson -> this.people.stream().filter(candidate -> currentPerson != candidate)
						.forEach(nextPerson -> pairs.add(Pair.create(currentPerson, nextPerson))));

		return pairs;
	}

	private Optional<Pair> getBestMatch(List<Pair> pairs, MatchCriteria matchCriteria) {

		return pairs.stream().reduce((currentPair, candidatePair) -> {
			if ((matchCriteria == MatchCriteria.MIN_AGE_DIFERENCE
					&& currentPair.getAgeDifference() < candidatePair.getAgeDifference())
					|| (matchCriteria == MatchCriteria.MAX_AGE_DIFERENCE
							&& currentPair.getAgeDifference() > candidatePair.getAgeDifference())) {
				return currentPair;
			} else {
				return candidatePair;
			}

		});

	}
}
