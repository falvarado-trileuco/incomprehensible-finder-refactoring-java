package algorithm;

public enum MatchCriteria {
	MIN_AGE_DIFERENCE, MAX_AGE_DIFERENCE;
}
