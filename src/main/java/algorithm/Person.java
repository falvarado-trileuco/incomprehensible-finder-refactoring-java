package algorithm;

import java.util.Date;
import java.util.GregorianCalendar;

public class Person {

	private String name;
	private Date birthDate;

	public Person(String name, int birthYear, int birthMonth, int birthDay) {
		this.name = name;
		this.birthDate = new GregorianCalendar(birthYear, birthMonth, birthDay).getTime();
	}

	public boolean isYounguerThan(Person other) {
		return this.birthDate.before(other.birthDate);
	}

	public long ageDifferenceWith(Person otherPerson) {
		return this.isYounguerThan(otherPerson) ? otherPerson.birthDate.getTime() - this.birthDate.getTime()
				: this.birthDate.getTime() - otherPerson.birthDate.getTime();
	}

}
