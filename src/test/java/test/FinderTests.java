package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import algorithm.Pair;
import algorithm.MatchCriteria;
import algorithm.Finder;
import algorithm.Person;

public class FinderTests {

	Person sue;
	Person greg;
	Person sarah;
	Person mike;

	@Before
	public void setup() {
		sue = new Person("Sue", 50, 0, 1);
		greg = new Person("Greg", 52, 5, 1);
		sarah = new Person("Sarah", 82, 0, 1);
		mike = new Person("Mike", 79, 0, 1);
	}

	@Test
	public void testReturnsEmptyResultsWhenGivenEmptyList() {
		List<Person> list = new ArrayList<Person>();

		Finder finder = new Finder(list);
		Optional<Pair> result = finder.find(MatchCriteria.MAX_AGE_DIFERENCE);

		assertTrue(result.isEmpty());
	}

	@Test
	public void testReturnsEmptyResultsWhenGivenOnePerson() {
		List<Person> list = new ArrayList<Person>();
		list.add(sue);

		Finder finder = new Finder(list);
		Optional<Pair> result = finder.find(MatchCriteria.MAX_AGE_DIFERENCE);

		assertTrue(result.isEmpty());
	}

	@Test
	public void testReturnsClosestPairForTwoPeople() {
		List<Person> list = new ArrayList<Person>();
		list.add(sue);
		list.add(greg);

		Finder finder = new Finder(list);
		Optional<Pair> result = finder.find(MatchCriteria.MAX_AGE_DIFERENCE);

		assertEquals(sue, result.get().getYoung());
		assertEquals(greg, result.get().getOld());
	}

	@Test
	public void testReturnsFurthestPairForTwoPeople() {
		List<Person> list = new ArrayList<Person>();
		list.add(mike);
		list.add(greg);

		Finder finder = new Finder(list);
		Optional<Pair> result = finder.find(MatchCriteria.MAX_AGE_DIFERENCE);

		assertEquals(greg, result.get().getYoung());
		assertEquals(mike, result.get().getOld());
	}

	@Test
	public void testReturnsFurthestPairForFourPeople() {
		List<Person> list = new ArrayList<Person>();
		list.add(sue);
		list.add(sarah);
		list.add(mike);
		list.add(greg);

		Finder finder = new Finder(list);
		Optional<Pair> result = finder.find(MatchCriteria.MAX_AGE_DIFERENCE);

		assertEquals(sue, result.get().getYoung());
		assertEquals(sarah, result.get().getOld());
	}

	@Test
	public void testReturnsClosestPairForFourPeople() {
		List<Person> list = new ArrayList<Person>();
		list.add(sue);
		list.add(sarah);
		list.add(mike);
		list.add(greg);

		Finder finder = new Finder(list);
		Optional<Pair> result = finder.find(MatchCriteria.MIN_AGE_DIFERENCE);

		assertEquals(sue, result.get().getYoung());
		assertEquals(greg, result.get().getOld());
	}

}
